package de.paxii.clarinet.module.external;

import de.paxii.clarinet.Wrapper;
import de.paxii.clarinet.event.EventHandler;
import de.paxii.clarinet.event.events.game.IngameTickEvent;
import de.paxii.clarinet.module.Module;
import de.paxii.clarinet.module.ModuleCategory;
import de.paxii.clarinet.util.chat.Chat;

/**
 * Created by Lars on 14.05.2016.
 */
public class ModuleLevelMacro extends Module {
  private boolean hasSentCommand;

  public ModuleLevelMacro() {
    super("LevelMacro", ModuleCategory.OTHER);

    this.setVersion("1.0");
    this.setBuildVersion(15801);

    this.setCommand(true);
    this.setDescription("Executes a Command when you reach a given level.");
    this.setSyntax("levelmacro <command / level> <command / level>");
  }

  @Override
  public void onEnable() {
    Wrapper.getEventManager().register(this);

    if (!this.getModuleSettings().containsKey("command")) {
      Chat.printClientMessage("You did not define a Command for LevelMacro. Use \"#levelmacro command <command>\".");
    }
    if (!this.getModuleSettings().containsKey("level")) {
      Chat.printClientMessage("You did not define a level for LevelMacro. Use \"#levelmacro level <level>\".");
    }
  }

  @EventHandler
  public void onIngameTick(IngameTickEvent event) {
    boolean hasExperience = Wrapper.getPlayer().experienceLevel >= Integer.parseInt(this.getModuleSettings().getOrDefault("level", "2000"));

    if (!this.hasSentCommand) {
      if (hasExperience) {
        Chat.sendChatMessage("/" + this.getModuleSettings().getOrDefault("command", "ping"));
        Chat.printClientMessage("LevelMacro: Command executed!");
        this.hasSentCommand = true;
      }
    } else {
      if (!hasExperience) {
        this.hasSentCommand = false;
      }
    }
  }

  @Override
  public void onCommand(String[] args) {
    if (args.length >= 2) {
      if (args[0].equalsIgnoreCase("command")) {
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 1; i < args.length; i++)
          stringBuilder.append(args[i]).append(" ");

        String command = stringBuilder.toString().trim();

        if (command.length() > 0) {
          if (command.startsWith("/"))
            command = command.substring(1);

          this.getModuleSettings().put("command", command);
          Chat.printClientMessage("Successfully set command for LevelMacro!");
        } else {
          Chat.printClientMessage("Cannot execute an empty Command!");
        }
      } else if (args[0].equalsIgnoreCase("level")) {
        try {
          int level = Integer.parseInt(args[1]);
          this.getModuleSettings().put("level", String.valueOf(level));

          Chat.printClientMessage("Successfully set level for LevelMacro!");
        } catch (NumberFormatException nfe) {
          Chat.printClientMessage("Invalid level!");
        }
      } else {
        Chat.printClientMessage("Invalid subcommand!");
      }
    } else {
      Chat.printClientMessage("Invalid Syntax!");
    }
  }

  @Override
  public void onDisable() {
    Wrapper.getEventManager().unregister(this);
  }
}
